# devout_sdl
A MacOS specific program to aid students learning how to code by giving them variadic input that fluctuates and is imperfect using a PS4 DualShock4 controller.

### USAGE
```
./devout_sdl
./devout_sdl 12
./devout_sdl 2 
./devout_sdl 2 4
./devout 3 1 5
```

The first line will print the available events the user can have printed to the terminal, or tell you that no PS4 controller is connected.
The second line prints the PS4's touchpad, sensors data and time to standard output.
The third line prints the PS4's axis data and time to standard output.
The fourth line prints the PS4's axis data, but only the triggers and time to standard output.
The fifth line prints buttons and axis but only dpad and joystick data along with time to standard output.

The output is a printed in a comma seperated columns of data corresponding to the choices given in the arguments run with the command.

![installing](./images/installing_devout_sdl.png)
![building](./images/building_devout_sdl.png)
![no args, no controller](./images/running_devout_sdl_no_args_no_controller.png)
![no args, no controller, output](./images/devout_sdl_no_args_no_controller_output.png)
![no args, with controller](./images/devout_sdl_output_no_args_with_controller.png)
![command for time and buttons](./images/devout_sdl_command_for_buttons_and_time.png)
![output for time and buttons](./images/devout_sdl_arg_1_output.png)
![output for 3 1 4 args](./images/devout_sdl_output_3_1_4.png)
![output for -3 1 4 args](./images/devout_sdl_output_negative_3_1_4.png)
