//standard library usage
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h> //using common but unspecified addons to the standard (PRIu32...etc)

//posix additions
#include <signal.h>
#include <unistd.h>

//external libraries
#include <SDL.h>

void HANDLE_SDL_QUIT(int signal);
void axis_event( const SDL_ControllerAxisEvent sdl_event );
void button_event( const SDL_ControllerButtonEvent sdl_event );
void touchpad_event( const SDL_TouchFingerEvent sdl_event );
void sensor_event( const SDL_ControllerSensorEvent sdl_sensor_event );

enum { 	
	BUTTONS = 1, 
		DPAD_ONLY = 1,
		SHAPES_ONLY = 2,
		ODD_BUTTONS_ONLY = 3,
	AXIS = 2, 
		TRIGGERS_ONLY = 4,
		JOYSTICKS_ONLY = 5,
	TOUCHPAD = 4, 
	SENSOR = 8,			//NOTE: Pretty sure gyro and accel are flipped in SDL
		ACCEL_ONLY = 6,
		GYRO_ONLY = 7,

	BUTTONS_AXIS = 3, 
	BUTTONS_TOUCHPAD = 5, 
	BUTTONS_SENSOR = 9, 
	AXIS_TOUCHPAD = 6, 
	AXIS_SENSOR = 10,
	TOUCHPAD_SENSOR = 12,

	BUTTONS_AXIS_TOUCHPAD = 7,
	BUTTONS_AXIS_SENSOR = 11,
	BUTTONS_TOUCHPAD_SENSOR = 13,
	AXIS_TOUCHPAD_SENSOR = 14,

	EVERYTHING = 15
};

//timestamp, 		type, 	     axis (i dont know how many yet), value, button (i dont know how many yet), state, 		x, 	y, sensor type, data (array of 3)
//double (casted), unsigned int, unsigned int, 				int, unsigned int, 			unsigned int, float, float, int,	 float[3]
double time;
unsigned int type;
float x, y, gyro[3], accel[3];
int sensor_type;
//axis
unsigned int left_joy_lr, left_joy_ud, right_joy_lr, right_joy_ud, left_trigger, right_trigger;
//buttons
unsigned int x_btn, circle_btn, triangle_btn, square_btn, right_joy_btn, left_joy_btn, touchpad_btn, dpad_up_btn, dpad_down_btn, dpad_right_btn, dpad_left_btn, ps_btn, share_btn, options_btn, r1_btn, l1_btn;

int main(int argc, char** argv)
{
	//SDL disables CTRL+C to stop a program by default, I make it exit the program again
	struct sigaction sa_struct;	
	sa_struct.sa_handler = &HANDLE_SDL_QUIT;
	sa_struct.sa_flags = SA_RESTART; //needed for some system calls and I think it does no harm for the ones that dont need it?!?!
	sigaction(SIGINT, &sa_struct, NULL);

	//initialize controller with sensor support (I think this is everything that I need)
	SDL_Init(SDL_INIT_GAMECONTROLLER | SDL_INIT_SENSOR | SDL_INIT_VIDEO); //video might be called implicitly by gamecontroller, but just in case because it is needed for input events

	//make sure at least one controller is connected (at the moment I only want to write code to support one controller)
	if (SDL_NumJoysticks() == 0) {
		system("reset");
		fprintf(stderr, "No DualShock4 controllers detected. This could be because of a loose or bad connection.\n");
		exit(-1);
	}

	//opens a controller and sets a pointer to it
	SDL_GameController* my_controller = SDL_GameControllerOpen(0);

	//determines what features the user wants
	if (argc == 1) {
		system("reset");
		char* device_string = SDL_GameControllerMapping(my_controller);
		if (strstr(device_string, "03008fe54c050000cc09000000016800") != NULL) {
			printf( "\n------------------------------------------------------------------------------\n"	\
				"A DualShock4 controller was found.\n\n\n"	\
				"Run the program again with an additional integer argument that denotes what features you want.\n"	\
				"Example: ./devout_sdl 12\n"			\
				"\n"
				"Options:\n\n"					\
				"Buttons:\t\t\t\t 1\n"				\
				"Axis:\t\t\t\t\t 2\n"				\
				"TouchPad:\t\t\t\t 4\n"				\
				"Sensors:\t\t\t\t 8\n"				\
				"\n"						\
				"Buttons and Axis:\t\t\t 3\n"			\
				"Buttons and Touchpad:\t\t\t 5\n"		\
				"Buttons and Sensors:\t\t\t 9\n"		\
				"Axis and Touchpad:\t\t\t 6\n"			\
				"Axis and Sensors:\t\t\t 10\n"			\
				"Touchpad and Sensors:\t\t\t 12\n"		\
				"\n"						\
				"Buttons, Axis, and Touchpad:\t\t 7\n"		\
				"Buttons, Axis, and Sensors:\t\t 11\n"		\
				"Buttons, Touchpad, and Sensors:\t\t 13\n"	\
				"Axis, Touchpad, and Sensors:\t\t 14\n"		\
				"\n"						\
				"Buttons, Axis, Touchpad, and Sensors:\t 15\n"	\
				"\n"						\
				"Note: whenever the \"Buttons\" flag is used, an additional second integer argument can be passed denoting which buttons you want.\n" \
				"By default all of the buttons are printed.\n"  \
				"Options for buttons:\n" 			\
				"DPAD only:\t\t\t\t 1\n"      			\
				"Shapes only:\t\t\t\t 2\n"			\
				"Other buttons only:\t\t\t 3\n"	\
				"\n\n"						\
				"Note: whenever the \"Axis\" flag is used, an additional second integer argument can be passed denoting which axis you want.\n" \
				"By default all of the axis are printed.\n"  \
				"Options for axis:\n" 				\
				"Triggers only:\t\t\t 4\n"			\
				"Joysticks only:\t\t\t 5\n"			\
				"\n\n"						\
				"Note: whenever the \"Sensors\" flag is used, an additional second integer argument can be passed denoting which sensor you want.\n" \
				"By default all sensors are printed.\n"		\
				"Options for sensors:\n"			\
				"Gyro only:\t\t\t 6\n"				\
				"Accel only:\t\t\t 7\n"				\
				"\n"						\
				"Example: ./devout_sdl 15 2 7 4\n"		\
				"\n"						\
				"In the example above 15 as the first argument denotes the user wants to use all the capabilities of the controller including: buttons, axis, touchpad and sensors.\n"	\
				"The following 2, 7 and 4 are options to limit the output of buttons, sensors, and axis respectfully.\n"	\
				"A thing to note is that the order of the arguments \"2 7 4\" doesn't matter.\n"					\
				"The 2 will limit the buttons to shapes only, the 7 will limit the sensor to accel only, and the 4 will limit the axis to triggers only.\n"	\
				"\n\n"	\
				"Note: to omit the time from being printed, make the first argument a negative number.\n"	\
				"Example: ./devout_sdl -15 2 7 4\n"		\
				"The above example is the same as before but the time column will be omitted.\n"		\
				"\n\n"						\
				"This program was written using Simple DirectMedia Layer 2  (SDL2). A cross platform hardware abstraction layer.\n"	\
				"\n------------------------------------------------------------------------------\n");
		}
		return 0;
	} 
	
	
	//read command line arguments
	//options are: buttons, axis, touchpad, sensor or any combination of them
	//		1	2	4	8	3, 5 - 7, 9 - 15
	char* end_ptr; //used for following strtol() functions
	long int arguments[4]; // 0 for features of device wanted, 1 - 3 for limiting what is printed of that/those features.
					
	int bool_dpad_only = 0, bool_shapes_only = 0, bool_other_buttons_only = 0, bool_triggers_only = 0, bool_joysticks_only = 0, bool_gyro_only = 0, bool_accel_only = 0, bool_time = 1;
	if (argc == 2) { //first arguument is the name of the command: "./devout_sdl"
		arguments[0] = strtol(argv[1],&end_ptr,10);
		if (arguments[0] < 0) {
			bool_time = 0;
			arguments[0] *= -1;
		}
		if (arguments[0] <= 0 || arguments[0] >= 16 ) {
			fprintf(stderr, "An invalid first integer argument was passed. Only numbers 1-15 are supported.\n");
			exit(-1);
		}
	} else if (argc > 2 && argc < 6 ) { //if they specify at least one option to limit the standard amount that is printed out no more than 5 options ever allowed (1 = ./devout_sdl, 2 = device feature, 3 - 5 (limiting buttons, axis and/or sensors)
		arguments[0] = strtol(argv[1],&end_ptr,10);
		if (arguments[0] < 0) {
			bool_time = 0;
			arguments[0] *= -1;
		}
		if (arguments[0] <= 0 || arguments[0] >= 16 ) {
			fprintf(stderr, "An invalid first integer argument was passed. Only numbers 1-15 are supported.\n");
			exit(-1);
		}
		for (int i = 0; i < argc - 2; ++i) {
			arguments[i + 1] = strtol(argv[i + 2],&end_ptr,10);
			if (arguments[i + 1] < 1 || arguments[i + 1] > 7) {
				fprintf(stderr, "An invalid argument was passed. Only numbers 1-7 are supported to limit output.\n");
				exit(-1);
			}
			switch(arguments[i + 1]) {
				case DPAD_ONLY:
					bool_dpad_only = 1;
					break;
				case SHAPES_ONLY:
					bool_shapes_only = 1;
					break;
				case ODD_BUTTONS_ONLY:
					bool_other_buttons_only = 1;
					break;
				case TRIGGERS_ONLY:
					bool_triggers_only = 1;
					break;
				case JOYSTICKS_ONLY:
					bool_joysticks_only = 1;
					break;
				case GYRO_ONLY:
					bool_gyro_only = 1;
					break;
				case ACCEL_ONLY:
					bool_accel_only = 1;
					break;
				default:
					fprintf(stderr, "Should never make it here when trying to parse arguments from the user.\n");
					exit(-1);
					break;
			}
		}
	} else {
		fprintf(stderr, "Too many arguments passed. Only one argument is allowed, like this: ./devout_sdl 12\n");
		exit(-1);
	}

	//enable sensors (gyro accel) for controller
	if (SDL_GameControllerSetSensorEnabled(my_controller, SDL_SENSOR_GYRO, SDL_TRUE) != 0) {
		fprintf(stderr, "An error occured trying to enable GYRO support.\n");
		exit(-1);
	}
	if (SDL_GameControllerSetSensorEnabled(my_controller, SDL_SENSOR_ACCEL, SDL_TRUE) != 0) {
		fprintf(stderr, "An error occured trying to enable ACCELEROMETER support.\n");
		exit(-1);
	}

	//based on the events the user wants to print, print out the events as they occur
	SDL_Event sdl_event;

	//switch for each option, long and tedious, but easier to implement than the (albeit better) generic controller approach)
	//The following commented out code can be added to every switch statement without a sensor so that it will print even if no new event is found.
	/*
						case SDL_CONTROLLERSENSORUPDATE: //included to update time since this updates automatically without user needing to press something, that way something is always printing on the screen
						case SDL_SENSORUPDATE:
							sensor_event( sdl_event.csensor );
							break;
	*/
	switch (arguments[0]) {
		case BUTTONS:
			while (1) {
    				while(SDL_PollEvent(&sdl_event)) {
					switch (sdl_event.type) {
						case SDL_CONTROLLERBUTTONDOWN: //these are defined SDL_events.h
        					case SDL_CONTROLLERBUTTONUP:
            						button_event( sdl_event.cbutton );
            						break;
						default: //will catch unneeded types
							break;
					}//switch(sdl_event.type)
				} //while(SDL_PollEvent())
				if (bool_time) {
					printf("%7.3lf, ", time);
				}
				if (bool_dpad_only) {
					printf("%d, %d, %d, %d, ", dpad_up_btn, dpad_down_btn, dpad_right_btn, dpad_left_btn);
				} else if (bool_shapes_only) {
					printf("%d, %d, %d, %d, ", x_btn, circle_btn, triangle_btn, square_btn);
				} else if (bool_other_buttons_only) {
					printf("%d, %d, %d, %d, %d, %d, %d, %d, ", right_joy_btn, left_joy_btn, touchpad_btn, ps_btn, share_btn, options_btn, r1_btn, l1_btn);
				} else {
					printf("%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, ", x_btn, circle_btn, triangle_btn, square_btn, right_joy_btn, left_joy_btn, touchpad_btn, dpad_up_btn, dpad_down_btn, dpad_right_btn, dpad_left_btn, ps_btn, share_btn, options_btn, r1_btn, l1_btn);
				}
				puts("");
				SDL_Delay(1);
				SDL_GameControllerUpdate();
			} //while(1)
			break; //should never make it here

		case AXIS:
			while (1) {
    				while(SDL_PollEvent(&sdl_event)) {
					switch (sdl_event.type) {
        					case SDL_CONTROLLERAXISMOTION:
            						axis_event( sdl_event.caxis );
            						break;
						default: //will catch unneeded types
							break;
					}//switch(sdl_event.type)
				} //while(SDL_PollEvent())
				if (bool_time) {
					printf("%7.3lf, ", time);
				}
				if (bool_triggers_only) {
					printf("%5d, %5d, ", left_trigger, right_trigger);
				} else if (bool_joysticks_only) {
					printf("% 6d, %6d, % 6d, % 6d, ", left_joy_lr, left_joy_ud, right_joy_lr, right_joy_ud);
				} else {
					printf("% 6d, %6d, % 6d, % 6d, %5d, %5d, ",left_joy_lr, left_joy_ud, right_joy_lr, right_joy_ud, left_trigger, right_trigger);
				}
				puts("");
				SDL_Delay(1);
				SDL_GameControllerUpdate();
			} //while(1)
			break; //should never make it here

		case TOUCHPAD:
			while (1) {
    				while(SDL_PollEvent(&sdl_event)) {
					switch (sdl_event.type) {
						case SDL_CONTROLLERTOUCHPADDOWN:
						case SDL_CONTROLLERTOUCHPADMOTION:
						case SDL_CONTROLLERTOUCHPADUP:
						case SDL_FINGERMOTION: 
						case SDL_FINGERDOWN:
						case SDL_FINGERUP:
							touchpad_event( sdl_event.tfinger );
							break;
						default: //will catch unneeded types
							break;
					}//switch(sdl_event.type)
				} //while(SDL_PollEvent())
				if (bool_time) {
					printf("%7.3lf, ", time);
				}
				printf("%lf, %lf, ", x, y);
				puts("");
				SDL_Delay(5);
				SDL_GameControllerUpdate();
			} //while(1)
			break; //should never make it here

		case SENSOR:
			while (1) {
    				while(SDL_PollEvent(&sdl_event)) {
					switch (sdl_event.type) {
						case SDL_CONTROLLERSENSORUPDATE:
						case SDL_SENSORUPDATE:
							sensor_event( sdl_event.csensor );
							break;
						default: //will catch unneeded types
							break;
					}//switch(sdl_event.type)
				} //while(SDL_PollEvent())
				if (bool_time) {
					printf("%7.3lf, ", time);
				}
				if (bool_gyro_only) {
					printf("% 10lf, % 10lf, % 10lf, ", gyro[0], gyro[1], gyro[2]);
				} else if (bool_accel_only) {
					printf("% 10lf, % 10lf, % 10lf, ", accel[0], accel[1], accel[2]);
				} else {
					printf("% 10lf, % 10lf, % 10lf, % 10lf, % 10lf, % 10lf, ", gyro[0], gyro[1], gyro[2], accel[0], accel[1], accel[2]);
				}
				puts("");
				SDL_Delay(2);
				SDL_GameControllerUpdate();
			} //while(1)
			break; //should never make it here

		case BUTTONS_AXIS:
			while (1) {
    				while(SDL_PollEvent(&sdl_event)) {
					switch (sdl_event.type) {
						case SDL_CONTROLLERBUTTONDOWN: //these are defined SDL_events.h
        					case SDL_CONTROLLERBUTTONUP:
            						button_event( sdl_event.cbutton );
            						break;
        					case SDL_CONTROLLERAXISMOTION:
            						axis_event( sdl_event.caxis );
            						break;
						default: //will catch unneeded types
							break;
					}//switch(sdl_event.type)
				} //while(SDL_PollEvent())
				if (bool_time) {
					printf("%7.3lf, ", time);
				}
				if (bool_dpad_only) {
					printf("%d, %d, %d, %d, ", dpad_up_btn, dpad_down_btn, dpad_right_btn, dpad_left_btn);
				} else if (bool_shapes_only) {
					printf("%d, %d, %d, %d, ", x_btn, circle_btn, triangle_btn, square_btn);
				} else if (bool_other_buttons_only) {
					printf("%d, %d, %d, %d, %d, %d, %d, %d, ", right_joy_btn, left_joy_btn, touchpad_btn, ps_btn, share_btn, options_btn, r1_btn, l1_btn);
				} else {
					printf("%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, ", x_btn, circle_btn, triangle_btn, square_btn, right_joy_btn, left_joy_btn, touchpad_btn, dpad_up_btn, dpad_down_btn, dpad_right_btn, dpad_left_btn, ps_btn, share_btn, options_btn, r1_btn, l1_btn);
				}
				if (bool_triggers_only) {
					printf("%5d, %5d, ", left_trigger, right_trigger);
				} else if (bool_joysticks_only) {
					printf("% 6d, %6d, % 6d, % 6d, ", left_joy_lr, left_joy_ud, right_joy_lr, right_joy_ud);
				} else {
					printf("% 6d, %6d, % 6d, % 6d, %5d, %5d, ",left_joy_lr, left_joy_ud, right_joy_lr, right_joy_ud, left_trigger, right_trigger);
				}
				puts("");
				SDL_Delay(1);
				SDL_GameControllerUpdate();
			} //while(1)
			break; //should never make it here

		case BUTTONS_TOUCHPAD:
			while (1) {
    				while(SDL_PollEvent(&sdl_event)) {
					switch (sdl_event.type) {
						case SDL_CONTROLLERBUTTONDOWN: //these are defined SDL_events.h
        					case SDL_CONTROLLERBUTTONUP:
            						button_event( sdl_event.cbutton );
            						break;
						case SDL_CONTROLLERTOUCHPADDOWN:
						case SDL_CONTROLLERTOUCHPADMOTION:
						case SDL_CONTROLLERTOUCHPADUP:
						case SDL_FINGERMOTION: 
						case SDL_FINGERDOWN:
						case SDL_FINGERUP:
							touchpad_event( sdl_event.tfinger );
							break;
						default: //will catch unneeded types
							break;
					}//switch(sdl_event.type)
				} //while(SDL_PollEvent())
				if (bool_time) {
					printf("%7.3lf, ", time);
				}
				if (bool_dpad_only) {
					printf("%d, %d, %d, %d, ", dpad_up_btn, dpad_down_btn, dpad_right_btn, dpad_left_btn);
				} else if (bool_shapes_only) {
					printf("%d, %d, %d, %d, ", x_btn, circle_btn, triangle_btn, square_btn);
				} else if (bool_other_buttons_only) {
					printf("%d, %d, %d, %d, %d, %d, %d, %d, ", right_joy_btn, left_joy_btn, touchpad_btn, ps_btn, share_btn, options_btn, r1_btn, l1_btn);
				} else {
					printf("%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, ", x_btn, circle_btn, triangle_btn, square_btn, right_joy_btn, left_joy_btn, touchpad_btn, dpad_up_btn, dpad_down_btn, dpad_right_btn, dpad_left_btn, ps_btn, share_btn, options_btn, r1_btn, l1_btn);
				}
				printf("%lf, %lf, ", x, y);
				puts("");
				SDL_Delay(2);
				SDL_GameControllerUpdate();
			} //while(1)
			break; //should never make it here

		case BUTTONS_SENSOR:
			while (1) {
    				while(SDL_PollEvent(&sdl_event)) {
					switch (sdl_event.type) {
						case SDL_CONTROLLERBUTTONDOWN: //these are defined SDL_events.h
        					case SDL_CONTROLLERBUTTONUP:
            						button_event( sdl_event.cbutton );
            						break;
						case SDL_CONTROLLERSENSORUPDATE:
						case SDL_SENSORUPDATE:
							sensor_event( sdl_event.csensor );
							break;
						default: //will catch unneeded types
							break;
					}//switch(sdl_event.type)
				} //while(SDL_PollEvent())
				if (bool_time) {
					printf("%7.3lf, ", time);
				}
				if (bool_dpad_only) {
					printf("%d, %d, %d, %d, ", dpad_up_btn, dpad_down_btn, dpad_right_btn, dpad_left_btn);
				} else if (bool_shapes_only) {
					printf("%d, %d, %d, %d, ", x_btn, circle_btn, triangle_btn, square_btn);
				} else if (bool_other_buttons_only) {
					printf("%d, %d, %d, %d, %d, %d, %d, %d, ", right_joy_btn, left_joy_btn, touchpad_btn, ps_btn, share_btn, options_btn, r1_btn, l1_btn);
				} else {
					printf("%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, ", x_btn, circle_btn, triangle_btn, square_btn, right_joy_btn, left_joy_btn, touchpad_btn, dpad_up_btn, dpad_down_btn, dpad_right_btn, dpad_left_btn, ps_btn, share_btn, options_btn, r1_btn, l1_btn);
				}
				if (bool_gyro_only) {
					printf("% 10lf, % 10lf, % 10lf, ", gyro[0], gyro[1], gyro[2]);
				} else if (bool_accel_only) {
					printf("% 10lf, % 10lf, % 10lf, ", accel[0], accel[1], accel[2]);
				} else {
					printf("% 10lf, % 10lf, % 10lf, % 10lf, % 10lf, % 10lf, ", gyro[0], gyro[1], gyro[2], accel[0], accel[1], accel[2]);
				}
				puts("");
				SDL_Delay(1);
				SDL_GameControllerUpdate();
			} //while(1)
			break; //should never make it here

		case AXIS_TOUCHPAD:
			while (1) {
    				while(SDL_PollEvent(&sdl_event)) {
					switch (sdl_event.type) {
        					case SDL_CONTROLLERAXISMOTION:
            						axis_event( sdl_event.caxis );
            						break;
						case SDL_CONTROLLERTOUCHPADDOWN:
						case SDL_CONTROLLERTOUCHPADMOTION:
						case SDL_CONTROLLERTOUCHPADUP:
						case SDL_FINGERMOTION: 
						case SDL_FINGERDOWN:
						case SDL_FINGERUP:
							touchpad_event( sdl_event.tfinger );
							break;
						default: //will catch unneeded types
							break;
					}//switch(sdl_event.type)
				} //while(SDL_PollEvent())
				if (bool_time) {
					printf("%7.3lf, ", time);
				}
				if (bool_triggers_only) {
					printf("%5d, %5d, ", left_trigger, right_trigger);
				} else if (bool_joysticks_only) {
					printf("% 6d, %6d, % 6d, % 6d, ", left_joy_lr, left_joy_ud, right_joy_lr, right_joy_ud);
				} else {
					printf("% 6d, %6d, % 6d, % 6d, %5d, %5d, ",left_joy_lr, left_joy_ud, right_joy_lr, right_joy_ud, left_trigger, right_trigger);
				}
				printf("%lf, %lf, ", x, y);
				puts("");
				SDL_Delay(2);
				SDL_GameControllerUpdate();
			} //while(1)
			break; //should never make it here

		case AXIS_SENSOR:
			while (1) {
    				while(SDL_PollEvent(&sdl_event)) {
					switch (sdl_event.type) {
        					case SDL_CONTROLLERAXISMOTION:
            						axis_event( sdl_event.caxis );
            						break;
						case SDL_CONTROLLERSENSORUPDATE:
						case SDL_SENSORUPDATE:
							sensor_event( sdl_event.csensor );
							break;
						default: //will catch unneeded types
							break;
					}//switch(sdl_event.type)
				} //while(SDL_PollEvent())
				if (bool_time) {
					printf("%7.3lf, ", time);
				}
				if (bool_triggers_only) {
					printf("%5d, %5d, ", left_trigger, right_trigger);
				} else if (bool_joysticks_only) {
					printf("% 6d, %6d, % 6d, % 6d, ", left_joy_lr, left_joy_ud, right_joy_lr, right_joy_ud);
				} else {
					printf("% 6d, %6d, % 6d, % 6d, %5d, %5d, ",left_joy_lr, left_joy_ud, right_joy_lr, right_joy_ud, left_trigger, right_trigger);
				}
				if (bool_gyro_only) {
					printf("% 10lf, % 10lf, % 10lf, ", gyro[0], gyro[1], gyro[2]);
				} else if (bool_accel_only) {
					printf("% 10lf, % 10lf, % 10lf, ", accel[0], accel[1], accel[2]);
				} else {
					printf("% 10lf, % 10lf, % 10lf, % 10lf, % 10lf, % 10lf, ", gyro[0], gyro[1], gyro[2], accel[0], accel[1], accel[2]);
				}
				puts("");
				SDL_Delay(2);
				SDL_GameControllerUpdate();
			} //while(1)
			break; //should never make it here

		case TOUCHPAD_SENSOR:
			while (1) {
    				while(SDL_PollEvent(&sdl_event)) {
					switch (sdl_event.type) {
						case SDL_CONTROLLERSENSORUPDATE:
						case SDL_SENSORUPDATE:
							sensor_event( sdl_event.csensor );
							break;
						case SDL_CONTROLLERTOUCHPADDOWN:
						case SDL_CONTROLLERTOUCHPADMOTION:
						case SDL_CONTROLLERTOUCHPADUP:
						case SDL_FINGERMOTION: 
						case SDL_FINGERDOWN:
						case SDL_FINGERUP:
							touchpad_event( sdl_event.tfinger );
							break;
						default: //will catch unneeded types
							break;
					}//switch(sdl_event.type)
				} //while(SDL_PollEvent())
				if (bool_time) {
					printf("%7.3lf, ", time);
				}
				printf("%lf, %lf, ", x, y);
				if (bool_gyro_only) {
					printf("% 10lf, % 10lf, % 10lf, ", gyro[0], gyro[1], gyro[2]);
				} else if (bool_accel_only) {
					printf("% 10lf, % 10lf, % 10lf, ", accel[0], accel[1], accel[2]);
				} else {
					printf("% 10lf, % 10lf, % 10lf, % 10lf, % 10lf, % 10lf, ", gyro[0], gyro[1], gyro[2], accel[0], accel[1], accel[2]);
				}
				puts("");
				SDL_Delay(2);
				SDL_GameControllerUpdate();
			} //while(1)
			break; //should never make it here

		case BUTTONS_AXIS_TOUCHPAD:
			while (1) {
    				while(SDL_PollEvent(&sdl_event)) {
					switch (sdl_event.type) {
						case SDL_CONTROLLERBUTTONDOWN: //these are defined SDL_events.h
        					case SDL_CONTROLLERBUTTONUP:
            						button_event( sdl_event.cbutton );
            						break;
        					case SDL_CONTROLLERAXISMOTION:
            						axis_event( sdl_event.caxis );
            						break;
						case SDL_CONTROLLERTOUCHPADDOWN:
						case SDL_CONTROLLERTOUCHPADMOTION:
						case SDL_CONTROLLERTOUCHPADUP:
						case SDL_FINGERMOTION: 
						case SDL_FINGERDOWN:
						case SDL_FINGERUP:
							touchpad_event( sdl_event.tfinger );
							break;
						default: //will catch unneeded types
							break;
					}//switch(sdl_event.type)
				} //while(SDL_PollEvent())
				if (bool_time) {
					printf("%7.3lf, ", time);
				}
				if (bool_dpad_only) {
					printf("%d, %d, %d, %d, ", dpad_up_btn, dpad_down_btn, dpad_right_btn, dpad_left_btn);
				} else if (bool_shapes_only) {
					printf("%d, %d, %d, %d, ", x_btn, circle_btn, triangle_btn, square_btn);
				} else if (bool_other_buttons_only) {
					printf("%d, %d, %d, %d, %d, %d, %d, %d, ", right_joy_btn, left_joy_btn, touchpad_btn, ps_btn, share_btn, options_btn, r1_btn, l1_btn);
				} else {
					printf("%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, ", x_btn, circle_btn, triangle_btn, square_btn, right_joy_btn, left_joy_btn, touchpad_btn, dpad_up_btn, dpad_down_btn, dpad_right_btn, dpad_left_btn, ps_btn, share_btn, options_btn, r1_btn, l1_btn);
				}
				if (bool_triggers_only) {
					printf("%5d, %5d, ", left_trigger, right_trigger);
				} else if (bool_joysticks_only) {
					printf("% 6d, %6d, % 6d, % 6d, ", left_joy_lr, left_joy_ud, right_joy_lr, right_joy_ud);
				} else {
					printf("% 6d, %6d, % 6d, % 6d, %5d, %5d, ",left_joy_lr, left_joy_ud, right_joy_lr, right_joy_ud, left_trigger, right_trigger);
				}
				printf("%lf, %lf, ", x, y);
				puts("");
				SDL_Delay(1);
				SDL_GameControllerUpdate();
			} //while(1)
			break; //should never make it here

		case BUTTONS_AXIS_SENSOR:
			while (1) {
    				while(SDL_PollEvent(&sdl_event)) {
					switch (sdl_event.type) {
						case SDL_CONTROLLERBUTTONDOWN: //these are defined SDL_events.h
        					case SDL_CONTROLLERBUTTONUP:
            						button_event( sdl_event.cbutton );
            						break;
        					case SDL_CONTROLLERAXISMOTION:
            						axis_event( sdl_event.caxis );
            						break;
						case SDL_CONTROLLERSENSORUPDATE:
						case SDL_SENSORUPDATE:
							sensor_event( sdl_event.csensor );
							break;
						default: //will catch unneeded types
							break;
					}//switch(sdl_event.type)
				} //while(SDL_PollEvent())
				if (bool_time) {
					printf("%7.3lf, ", time);
				}
				if (bool_dpad_only) {
					printf("%d, %d, %d, %d, ", dpad_up_btn, dpad_down_btn, dpad_right_btn, dpad_left_btn);
				} else if (bool_shapes_only) {
					printf("%d, %d, %d, %d, ", x_btn, circle_btn, triangle_btn, square_btn);
				} else if (bool_other_buttons_only) {
					printf("%d, %d, %d, %d, %d, %d, %d, %d, ", right_joy_btn, left_joy_btn, touchpad_btn, ps_btn, share_btn, options_btn, r1_btn, l1_btn);
				} else {
					printf("%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, ", x_btn, circle_btn, triangle_btn, square_btn, right_joy_btn, left_joy_btn, touchpad_btn, dpad_up_btn, dpad_down_btn, dpad_right_btn, dpad_left_btn, ps_btn, share_btn, options_btn, r1_btn, l1_btn);
				}
				if (bool_triggers_only) {
					printf("%5d, %5d, ", left_trigger, right_trigger);
				} else if (bool_joysticks_only) {
					printf("% 6d, %6d, % 6d, % 6d, ", left_joy_lr, left_joy_ud, right_joy_lr, right_joy_ud);
				} else {
					printf("% 6d, %6d, % 6d, % 6d, %5d, %5d, ",left_joy_lr, left_joy_ud, right_joy_lr, right_joy_ud, left_trigger, right_trigger);
				}
				if (bool_gyro_only) {
					printf("% 10lf, % 10lf, % 10lf, ", gyro[0], gyro[1], gyro[2]);
				} else if (bool_accel_only) {
					printf("% 10lf, % 10lf, % 10lf, ", accel[0], accel[1], accel[2]);
				} else {
					printf("% 10lf, % 10lf, % 10lf, % 10lf, % 10lf, % 10lf, ", gyro[0], gyro[1], gyro[2], accel[0], accel[1], accel[2]);
				}
				puts("");
				SDL_Delay(1);
				SDL_GameControllerUpdate();
			} //while(1)
			break; //should never make it here

		case BUTTONS_TOUCHPAD_SENSOR:
			while (1) {
    				while(SDL_PollEvent(&sdl_event)) {
					switch (sdl_event.type) {
						case SDL_CONTROLLERBUTTONDOWN: //these are defined SDL_events.h
        					case SDL_CONTROLLERBUTTONUP:
            						button_event( sdl_event.cbutton );
            						break;
						case SDL_CONTROLLERSENSORUPDATE:
						case SDL_SENSORUPDATE:
							sensor_event( sdl_event.csensor );
							break;
						case SDL_CONTROLLERTOUCHPADDOWN:
						case SDL_CONTROLLERTOUCHPADMOTION:
						case SDL_CONTROLLERTOUCHPADUP:
						case SDL_FINGERMOTION: 
						case SDL_FINGERDOWN:
						case SDL_FINGERUP:
							touchpad_event( sdl_event.tfinger );
							break;
						default: //will catch unneeded types
							break;
					}//switch(sdl_event.type)
				} //while(SDL_PollEvent())
				if (bool_time) {
					printf("%7.3lf, ", time);
				}
				if (bool_dpad_only) {
					printf("%d, %d, %d, %d, ", dpad_up_btn, dpad_down_btn, dpad_right_btn, dpad_left_btn);
				} else if (bool_shapes_only) {
					printf("%d, %d, %d, %d, ", x_btn, circle_btn, triangle_btn, square_btn);
				} else if (bool_other_buttons_only) {
					printf("%d, %d, %d, %d, %d, %d, %d, %d, ", right_joy_btn, left_joy_btn, touchpad_btn, ps_btn, share_btn, options_btn, r1_btn, l1_btn);
				} else {
					printf("%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, ", x_btn, circle_btn, triangle_btn, square_btn, right_joy_btn, left_joy_btn, touchpad_btn, dpad_up_btn, dpad_down_btn, dpad_right_btn, dpad_left_btn, ps_btn, share_btn, options_btn, r1_btn, l1_btn);
				}
				printf("%lf, %lf, ", x, y);
				if (bool_gyro_only) {
					printf("% 10lf, % 10lf, % 10lf, ", gyro[0], gyro[1], gyro[2]);
				} else if (bool_accel_only) {
					printf("% 10lf, % 10lf, % 10lf, ", accel[0], accel[1], accel[2]);
				} else {
					printf("% 10lf, % 10lf, % 10lf, % 10lf, % 10lf, % 10lf, ", gyro[0], gyro[1], gyro[2], accel[0], accel[1], accel[2]);
				}
				puts("");
				SDL_Delay(1);
				SDL_GameControllerUpdate();
			} //while(1)
			break; //should never make it here

		case AXIS_TOUCHPAD_SENSOR:
			while (1) {
    				while(SDL_PollEvent(&sdl_event)) {
					switch (sdl_event.type) {
        					case SDL_CONTROLLERAXISMOTION:
            						axis_event( sdl_event.caxis );
            						break;
						case SDL_CONTROLLERSENSORUPDATE:
						case SDL_SENSORUPDATE:
							sensor_event( sdl_event.csensor );
							break;
						case SDL_CONTROLLERTOUCHPADDOWN:
						case SDL_CONTROLLERTOUCHPADMOTION:
						case SDL_CONTROLLERTOUCHPADUP:
						case SDL_FINGERMOTION: 
						case SDL_FINGERDOWN:
						case SDL_FINGERUP:
							touchpad_event( sdl_event.tfinger );
							break;
						default: //will catch unneeded types
							break;
					}//switch(sdl_event.type)
				} //while(SDL_PollEvent())
				if (bool_time) {
					printf("%7.3lf, ", time);
				}
				if (bool_triggers_only) {
					printf("%5d, %5d, ", left_trigger, right_trigger);
				} else if (bool_joysticks_only) {
					printf("% 6d, %6d, % 6d, % 6d, ", left_joy_lr, left_joy_ud, right_joy_lr, right_joy_ud);
				} else {
					printf("% 6d, %6d, % 6d, % 6d, %5d, %5d, ",left_joy_lr, left_joy_ud, right_joy_lr, right_joy_ud, left_trigger, right_trigger);
				}
				printf("%lf, %lf, ", x, y);
				if (bool_gyro_only) {
					printf("% 10lf, % 10lf, % 10lf, ", gyro[0], gyro[1], gyro[2]);
				} else if (bool_accel_only) {
					printf("% 10lf, % 10lf, % 10lf, ", accel[0], accel[1], accel[2]);
				} else {
					printf("% 10lf, % 10lf, % 10lf, % 10lf, % 10lf, % 10lf, ", gyro[0], gyro[1], gyro[2], accel[0], accel[1], accel[2]);
				}
				puts("");
				SDL_Delay(1);
				SDL_GameControllerUpdate();
			} //while(1)
			break; //should never make it here

		case EVERYTHING:
			while (1) {
    				while(SDL_PollEvent(&sdl_event)) {
					switch (sdl_event.type) {
						case SDL_CONTROLLERBUTTONDOWN: //these are defined SDL_events.h
        					case SDL_CONTROLLERBUTTONUP:
            						button_event( sdl_event.cbutton );
            						break;
        					case SDL_CONTROLLERAXISMOTION:
            						axis_event( sdl_event.caxis );
            						break;
						case SDL_CONTROLLERSENSORUPDATE:
						case SDL_SENSORUPDATE:
							sensor_event( sdl_event.csensor );
							break;
						case SDL_CONTROLLERTOUCHPADDOWN:
						case SDL_CONTROLLERTOUCHPADMOTION:
						case SDL_CONTROLLERTOUCHPADUP:
						case SDL_FINGERMOTION: 
						case SDL_FINGERDOWN:
						case SDL_FINGERUP:
							touchpad_event( sdl_event.tfinger );
							break;
						default: //will catch unneeded types
							break;
					}//switch(sdl_event.type)
				} //while(SDL_PollEvent())
				if (bool_time) {
					printf("%7.3lf, ", time);
				}
				if (bool_dpad_only) {
					printf("%d, %d, %d, %d, ", dpad_up_btn, dpad_down_btn, dpad_right_btn, dpad_left_btn);
				} else if (bool_shapes_only) {
					printf("%d, %d, %d, %d, ", x_btn, circle_btn, triangle_btn, square_btn);
				} else if (bool_other_buttons_only) {
					printf("%d, %d, %d, %d, %d, %d, %d, %d, ", right_joy_btn, left_joy_btn, touchpad_btn, ps_btn, share_btn, options_btn, r1_btn, l1_btn);
				} else {
					printf("%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, ", x_btn, circle_btn, triangle_btn, square_btn, right_joy_btn, left_joy_btn, touchpad_btn, dpad_up_btn, dpad_down_btn, dpad_right_btn, dpad_left_btn, ps_btn, share_btn, options_btn, r1_btn, l1_btn);
				}
				if (bool_triggers_only) {
					printf("%5d, %5d, ", left_trigger, right_trigger);
				} else if (bool_joysticks_only) {
					printf("% 6d, %6d, % 6d, % 6d, ", left_joy_lr, left_joy_ud, right_joy_lr, right_joy_ud);
				} else {
					printf("% 6d, %6d, % 6d, % 6d, %5d, %5d, ",left_joy_lr, left_joy_ud, right_joy_lr, right_joy_ud, left_trigger, right_trigger);
				}
				printf("%lf, %lf, ", x, y);
				if (bool_gyro_only) {
					printf("% 10lf, % 10lf, % 10lf, ", gyro[0], gyro[1], gyro[2]);
				} else if (bool_accel_only) {
					printf("% 10lf, % 10lf, % 10lf, ", accel[0], accel[1], accel[2]);
				} else {
					printf("% 10lf, % 10lf, % 10lf, % 10lf, % 10lf, % 10lf, ", gyro[0], gyro[1], gyro[2], accel[0], accel[1], accel[2]);
				}
				puts("");
				SDL_Delay(1);
				SDL_GameControllerUpdate();
			} //while(1)
			break; //should never make it here
		default:
			fprintf(stderr, "Should not make it here from the argument switch statement!\n");
			exit(-1);
	} //switch(argument)

	return 0;
}


void HANDLE_SDL_QUIT(int signal)
{
	printf("\n");
	exit(1);
}

void button_event( const SDL_ControllerButtonEvent sdl_event )
{
//buttons
//x_btn, circle_btn, triangle_btn, square_btn, right_joy_btn, left_joy_btn, touchpad_btn, dpad_up_btn, dpad_down_btn, dpad_right_btn, dpad_left_btn, ps_btn, share_btn, options_btn, r1_btn, l1_btn;
	//time, button, state
	time = (double) sdl_event.timestamp / 1000;
	switch (sdl_event.button) {
		case SDL_CONTROLLER_BUTTON_A: //found in controller.h
			x_btn = sdl_event.state;
			break;
		case SDL_CONTROLLER_BUTTON_B:
			circle_btn = sdl_event.state;
			break;
		case SDL_CONTROLLER_BUTTON_X:
			square_btn = sdl_event.state;
			break;
		case SDL_CONTROLLER_BUTTON_Y:
			triangle_btn = sdl_event.state;
			break;
		case SDL_CONTROLLER_BUTTON_BACK:
			share_btn = sdl_event.state;
			break;
		case SDL_CONTROLLER_BUTTON_GUIDE:
			ps_btn = sdl_event.state;
			break;
		case SDL_CONTROLLER_BUTTON_START:
			options_btn = sdl_event.state;
			break;
		case SDL_CONTROLLER_BUTTON_LEFTSTICK:
			left_joy_btn = sdl_event.state;
			break;
		case SDL_CONTROLLER_BUTTON_RIGHTSTICK:
			right_joy_btn = sdl_event.state;
			break;
		case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
			l1_btn = sdl_event.state;
			break;
		case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
			r1_btn = sdl_event.state;
			break;
		case SDL_CONTROLLER_BUTTON_DPAD_UP:
			dpad_up_btn = sdl_event.state;
			break;
		case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
			dpad_down_btn = sdl_event.state;
			break;
		case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
			dpad_left_btn = sdl_event.state;
			break;
		case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
			dpad_right_btn = sdl_event.state;
			break;
		case SDL_CONTROLLER_BUTTON_TOUCHPAD:
			touchpad_btn = sdl_event.state;
			break;
		default:
			break;
	}
}

void axis_event( const SDL_ControllerAxisEvent sdl_event )
{
//axis
//left_joy_lr, left_joy_ud, right_joy_lr, right_joy_ud, left_trigger, right_trigger;
//    
	//time, axis, value
	time = (double) sdl_event.timestamp / 1000;
       	switch (sdl_event.axis) {
		case SDL_CONTROLLER_AXIS_LEFTX: //found in controller.h
			left_joy_lr = sdl_event.value;
			break;
		case SDL_CONTROLLER_AXIS_LEFTY:
			left_joy_ud = sdl_event.value;
			break;
		case SDL_CONTROLLER_AXIS_RIGHTX:
			right_joy_lr = sdl_event.value;
			break;
		case SDL_CONTROLLER_AXIS_RIGHTY:
			right_joy_ud = sdl_event.value;
			break;
		case SDL_CONTROLLER_AXIS_TRIGGERLEFT:
			left_trigger = sdl_event.value;
			break;
		case SDL_CONTROLLER_AXIS_TRIGGERRIGHT:
			right_trigger = sdl_event.value;
			break;
		default:
			break;
	}
}

void touchpad_event( const SDL_TouchFingerEvent sdl_event )
{
	//time, x, y (currently the y value does not work)
	time = (double) sdl_event.timestamp / 1000;
       	x = sdl_event.x; 
	y = sdl_event.y;
}

void sensor_event( const SDL_ControllerSensorEvent sdl_event )
{
	//time, sensor type, data (array of 3)
	time = (double) sdl_event.timestamp / 1000;
	if (sdl_event.sensor == SDL_SENSOR_GYRO) {
		gyro[0] = sdl_event.data[0];
		gyro[1] = sdl_event.data[1];
		gyro[2] = sdl_event.data[2];
	} else if (sdl_event.sensor == SDL_SENSOR_ACCEL) {
		accel[0] = sdl_event.data[0];
		accel[1] = sdl_event.data[1];
		accel[2] = sdl_event.data[2];
	}
}
