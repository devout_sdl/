#include <stdio.h>
#include <unistd.h>

int main(void)
{
	double time;
	int left_trigger, right_trigger;

	int right_pressed = 0, left_pressed = 0;
	while(1) {
		if (scanf("%lf, %d, %d,", &time, &left_trigger, &right_trigger) != 3) {
			printf("scanf failed\n");
		}
		if (right_trigger > 1000 && right_pressed == 0) {
			right_pressed = 1;
			printf("Right trigger pressed at: %lf\n", time);
		} else if (right_trigger < 100) {
			right_pressed = 0;
 		}	
		  
		if (left_trigger > 1000 && left_pressed == 0) {
			left_pressed = 1;
			printf("Left trigger pressed at: %lf\n", time);
		}  else if (left_trigger < 100) {
			left_pressed = 0;
		}
	}

	return 0;
}
